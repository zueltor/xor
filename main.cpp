#include <cmath>
#include <iostream>
#include <vector>
#include <random>
#include <ctime>

#define LR 0.09
#define EPOCHS 30000
clock_t begin,end;
clock_t total=0;

static inline float sigmoid(float x) {
    return 1 / (1 + std::exp(-x));
}

static inline float sigmoid_deriv(float x) {
    return sigmoid(x) * (1 - sigmoid(x));
}

class Matrix {
public:
    float **a;
    int rows_count;
    int columns_count;
public:
    explicit Matrix(int m, int n, bool random = false) : rows_count(m), columns_count(n), a(new float *[m]) {
        for (int i = 0; i < rows_count; i++) {
            a[i] = new float[columns_count];
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = (random) ? (float) rand() / RAND_MAX - 0.5 : 0;
            }
        }
    }

    explicit Matrix(const std::vector<std::vector<float>> &values) : rows_count(values.size()),
                                                                     columns_count(values[0].size()),
                                                                     a(new float *[values.size()]) {
        for (int i = 0; i < rows_count; i++) {
            a[i] = new float[columns_count];
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = values[i][j];
            }
        }
    }

    Matrix(const Matrix &m) {
        rows_count = m.rows_count;
        columns_count = m.columns_count;
        a = new float *[rows_count];
        for (int i = 0; i < rows_count; i++) {
            a[i] = new float[columns_count];
            for (int j = 0; j < columns_count; j++) {
                a[i][j] = m.a[i][j];
            }
        }
    }

    Matrix sigmoid() const {
        Matrix res(rows_count, columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = ::sigmoid(a[i][j]);
            }
        }
        return res;
    }

    Matrix sigmoid_deriv() const {
        Matrix res(rows_count, columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = ::sigmoid_deriv(a[i][j]);
            }
        }
        return res;
    }

    Matrix &operator=(const Matrix &m) {
        bool different_rows = rows_count != m.rows_count;
        bool different = columns_count != m.columns_count || different_rows;
        if (this != &m) {
            if (different) {
                for (int i = 0; i < rows_count; i++) {
                    delete[] a[i];
                }
            }

            if (different_rows) {
                delete[] a;
                a = new float *[m.rows_count];
            }

            rows_count = m.rows_count;
            columns_count = m.columns_count;

            for (int i = 0; i < rows_count; i++) {
                if (different) { a[i] = new float[columns_count]; }

                for (int j = 0; j < columns_count; j++) {
                    a[i][j] = m.a[i][j];
                }
            }
        }
        return *this;
//        if (this != &m) {
//            for (int i = 0; i < rows_count; i++) {
//                delete[] a[i];
//            }
//            delete[] a;
//            rows_count = m.rows_count;
//            columns_count = m.columns_count;
//            a = new float *[rows_count];
//            for (int i = 0; i < rows_count; i++) {
//                a[i] = new float[columns_count];
//                for (int j = 0; j < columns_count; j++) {
//                    a[i][j] = m.a[i][j];
//                }
//            }
//        }
//        return *this;
    }

    Matrix concatenateColumn(std::vector<float> column) {
        Matrix res(rows_count, columns_count + 1);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 1; j < res.columns_count; j++) {
                res.a[i][j] = a[i][j - 1];
            }
            res.a[i][0] = column[i];
        }
        return res;
    }

    Matrix unconcatenateRow() const {
        Matrix res(rows_count - 1, columns_count);
        for (int i = 0; i < res.rows_count; i++) {
            for (int j = 0; j < res.columns_count; j++) {
                res.a[i][j] = a[i + 1][j];
            }
        }
        return res;
    }

    Matrix multiply(const Matrix &m) const {
        Matrix res(rows_count, m.columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                for (int k = 0; k < m.columns_count; k++) {
                    res.a[i][k] += a[i][j] * m.a[j][k];
                }
            }
        }
        return res;
    }

    int rows() const {
        return rows_count;
    }

    int columns() const {
        return columns_count;
    }

    Matrix operator*(const Matrix &m) const {
        if (rows_count != m.rows_count || columns_count != m.columns_count) {
            throw std::runtime_error("Incompatible matrix sizes\n");
        }
        Matrix res(rows_count, m.columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = a[i][j] * m.a[i][j];
            }
        }
        return res;
    }

    Matrix operator-(const Matrix &m) const {
        if (rows_count != m.rows_count || columns_count != m.columns_count) {
            throw std::runtime_error("Incompatible matrix sizes\n");
        }
        Matrix res(rows_count, m.columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = a[i][j] - m.a[i][j];
            }
        }
        return res;
    }

    Matrix operator^(int n) const {
        Matrix res(rows_count, columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = a[i][j] * a[i][j];
            }
        }
        return res;
    }

    Matrix operator*(float n) const{
        Matrix res(rows_count, columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = a[i][j] * n;
            }
        }
        return res;
    }

    Matrix T() const {
        Matrix res(columns_count, rows_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[j][i] = a[i][j];
            }
        }
        return res;
    }

    Matrix negPlusOne() const {
        Matrix res(rows_count, columns_count);
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res.a[i][j] = 1 - a[i][j];
            }
        }
        return res;
    }

    void print() {
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                printf("%f ", a[i][j]);
            }
            printf("\n");
        }
        printf("\n");
    }

    float sum() {
        float res = 0;
        for (int i = 0; i < rows_count; i++) {
            for (int j = 0; j < columns_count; j++) {
                res += a[i][j];
            }
        }
        return res;
    }

    ~Matrix() {
        for (int i = 0; i < rows_count; i++) {
            delete[] a[i];
        }
        delete[] a;
    }
};

void forward(const Matrix &X, const Matrix &W1, const Matrix &W2, Matrix &H_in, Matrix &H, Matrix &Y_in, Matrix &Y,const std::vector<float> &bias) {
    H_in = X.multiply(W1);
    H = H_in.sigmoid();
    H = H.concatenateColumn(bias);
    Y_in = H.multiply(W2);
    Y = Y_in.sigmoid();
}

void backprop(const Matrix &H_in, const Matrix &X, const Matrix &H, const Matrix &Y,
        const Matrix &T, const Matrix &W2, Matrix &E_sq, Matrix &grad_W1,
              Matrix &grad_W2) {

    Matrix E = Y - T;
    E_sq = E ^ 2;
    Matrix grad_Y = E * 2;
    Matrix grad_Y_in = Y * Y.negPlusOne() * grad_Y;
    grad_W2 = H.T().multiply(grad_Y_in);
    Matrix grad_H = grad_Y_in.multiply(W2.unconcatenateRow().unconcatenateRow());
    Matrix grad_H_in = grad_H * H_in.sigmoid_deriv();
    grad_W1 = X.T().multiply(grad_H_in);
}

int main() {
    srand(time(NULL));

    Matrix T({{0, 0, 0},
              {1, 0, 1},
              {1, 0, 1},
              {0, 1, 1}});
    Matrix W1(3, 2, true);
    Matrix W2(3, 3, true);

    Matrix X({{1, 0, 0},
              {1, 0, 1},
              {1, 1, 0},
              {1, 1, 1}});
    Matrix H_in(X.rows(),
                W1.columns()),
            H(X.rows(),
              W1.columns()),
            Y_in(H.rows(), W2.columns()),
            Y(Y_in.rows(), Y_in.columns()),
            E_sq(Y.rows(), Y.columns()),
            grad_W1(X.columns(), H.columns()),
            grad_W2(H.columns(), Y.columns());

    float alpha=LR*1.0/X.rows();
    std::vector<float> bias(H.rows(), 1);
    begin=clock();
    for (int i = 0; i < EPOCHS; i++) {
        forward(X, W1, W2, H_in, H, Y_in, Y,bias);
        backprop(H_in, X, H, Y, T, W2, E_sq, grad_W1, grad_W2);
        W1 = W1 - (grad_W1 * alpha);
        W2 = W2 - (grad_W2 * alpha);
//        c=E_sq.sum();
//        if (i%1000==0){
//            printf("Iteration %d, Error %f\n",i,c);
//            Y.print();
//        }
    }
    end=clock();
    total+=end-begin;
    double elapsed_secs = double(total) / CLOCKS_PER_SEC;
    Y.print();
    W1.print();
    W2.print();
    printf("Time %f\n", elapsed_secs);

    return 0;
}
